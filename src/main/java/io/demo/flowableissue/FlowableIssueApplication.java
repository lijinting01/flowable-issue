package io.demo.flowableissue;

import lombok.extern.slf4j.Slf4j;
import org.flowable.engine.RepositoryService;
import org.flowable.engine.repository.Deployment;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.List;

/**
 * @author lijinting01
 */
@Slf4j
@SpringBootApplication
public class FlowableIssueApplication {

	public static void main(String[] args) {
		SpringApplication.run(FlowableIssueApplication.class, args);
	}

	@Bean
	public InitializingBean initializingBean(RepositoryService repositoryService) {
		return () -> {
			List<Deployment> list = repositoryService.createDeploymentQuery().list();
			log.info("Deployment size: {}", list.size());// where '1' is expected
		};
	}

}
