package io.demo.flowableissue;

import org.flowable.engine.RepositoryService;
import org.flowable.engine.repository.Deployment;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class FlowableIssueApplicationTests {

	private RepositoryService repositoryService;

	@Test
	public void contextLoads() {
		List<Deployment> list = repositoryService.createDeploymentQuery().list();
		assertThat(list).hasSize(1); // the unit test is always OK
	}

	@Autowired
	public void setRepositoryService(RepositoryService repositoryService) {
		this.repositoryService = repositoryService;
	}
}
